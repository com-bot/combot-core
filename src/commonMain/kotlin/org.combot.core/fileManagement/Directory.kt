package org.combot.core.fileManagement

/** Base interface that represents a directory. */
public interface Directory : File {
    /**
     * Lists files that are in the directory. This function doesn't list files recursively.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. listing - Contains all files that are in this directory.
     */
    public suspend fun listFiles(): Pair<Boolean, Array<File>>
}
