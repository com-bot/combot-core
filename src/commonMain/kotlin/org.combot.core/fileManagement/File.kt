package org.combot.core.fileManagement

import org.combot.core.fileManagement.authorization.FilePermissions

/** Base interface for representing a file. */
public interface File {
    /** The path to the file's parent directory. */
    public val parentDirPath: String
    /** The name of the file. */
    public val name: String

    /**
     * Checks to see if this [File] is valid (eg does it exist).
     * @return A value of *true* if this [File] is valid.
     */
    public suspend fun isValid(): Boolean

    /**
     * Gets the size of this [File] in bytes.
     * @return The size of the file, or *-1L* if an error occurred.
     */
    public suspend fun size(): Long

    /**
     * Creates a new file.
     * @return A value of *true* if the file was created successfully.
     */
    public suspend fun create(): Boolean

    /**
     * Deletes the file (if it exists).
     * @return A value of *true* if the file was deleted successfully.
     */
    public suspend fun delete(): Boolean

    /**
     * Makes a copy of this file.
     * @param newFile The new file that will be the copy of this file.
     * @return A value of *true* if this file was successfully copied.
     */
    public suspend fun copy(newFile: File): Boolean

    /**
     * Moves this file.
     * @param newFile The file that will represent the moved file.
     * @return A value of *true* if this file was successfully moved.
     */
    public suspend fun move(newFile: File): Boolean

    /**
     * Changes the file's user.
     * @param user The new user for this file.
     * @return A value of *true* if the user was successfully changed.
     */
    public suspend fun changeUser(user: String): Boolean

    /**
     * Gets the file's user.
     * @return The user or *""* (an empty [String]) if an error occurred.
     */
    public suspend fun fetchUser(): String

    /**
     * Gets the file's user ID (uid).
     * @return The user ID or *-1* if an error occurred.
     */
    public suspend fun fetchUserId(): Int

    /**
     * Changes the file's group.
     * @param group The new group for this file.
     * @return A value of *true* if the group was successfully changed.
     */
    public suspend fun changeGroup(group: String): Boolean

    /**
     * Gets the file's group.
     * @return The group or *""* (an empty [String]) if an error occurred.
     */
    public suspend fun fetchGroup(): String

    /**
     * Gets the file's group ID (gid).
     * @return The group ID or *-1* if an error occurred.
     */
    public suspend fun fetchGroupId(): Int

    /**
     * Changes the file's permissions.
     * @param permissions The new permissions for this file.
     * @return A value of *true* if the file's permissions was successfully changed.
     */
    public suspend fun changePermissions(permissions: FilePermissions): Boolean
}
