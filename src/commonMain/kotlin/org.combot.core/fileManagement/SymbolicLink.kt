package org.combot.core.fileManagement

/** Base interface that represents a symbolic link (is a shortcut to a file which might not exist). */
public interface SymbolicLink : File {
    /** The destination file that [SymbolicLink] points to. */
    public val target: File
}
