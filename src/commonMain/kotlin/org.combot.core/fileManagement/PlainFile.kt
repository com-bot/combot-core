package org.combot.core.fileManagement

/** Base interface for representing a plain file, which is either a text or binary file. */
@ExperimentalUnsignedTypes
public interface PlainFile : File {
    /** If *true* then this file is a binary file, however if *false* then this file is a text file. */
    public val isBinaryFile: Boolean

    /**
     * Reads all text from this file.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - Contains all text read from the file. Each element represents a line.
     */
    public suspend fun readAllText(): Pair<Boolean, Array<String>>

    /**
     * Reads some text from this file.
     * @param size How much text to read in bytes.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - Contains some text read from the file.
     */
    public suspend fun readText(size: ULong): Pair<Boolean, String>

    /**
     * Reads all binary data from this file.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - Contains all binary data read from the file.
     */
    public suspend fun readAllBinary(): Pair<Boolean, Array<Byte>>

    /**
     * Reads some binary data from this file.
     * @param size How much binary data to read in bytes.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - Contains some binary data read from the file.
     */
    public suspend fun readBinary(size: ULong): Pair<Boolean, Array<Byte>>

    /**
     * Writes some text to this file.
     * @param data The text to write.
     * @return A value of *true* if the operation was successful.
     */
    public suspend fun writeText(vararg data: String): Boolean

    /**
     * Appends some text to this file.
     * @param data The text to append.
     * @return A value of *true* if the operation was successful.
     */
    public suspend fun appendText(vararg data: String): Boolean

    /**
     * Writes some [binary data][data] to this file.
     * @param data The binary data to write.
     * @return A value of *true* if the operation was successful.
     */
    public suspend fun writeBinary(vararg data: Byte): Boolean

    /**
     * Appends some [binary data][data] to this file.
     * @param data The binary data to append.
     * @return A value of *true* if the operation was successful.
     */
    public suspend fun appendBinary(vararg data: Byte): Boolean
}
