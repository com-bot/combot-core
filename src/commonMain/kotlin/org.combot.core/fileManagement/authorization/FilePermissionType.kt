package org.combot.core.fileManagement.authorization

@ExperimentalUnsignedTypes
/**
 * Indicates the file permission type to use, which is applied to **owner**, **group**, or **other** on a file.
 * @param num The octal representation of [FilePermissionType].
 */
public enum class FilePermissionType(public val num: UInt) {
    NONE(0u),
    EXEC(1u),
    WRITE(2u),
    EXEC_WRITE(3u),
    READ(4u),
    READ_EXEC(5u),
    READ_WRITE(6u),
    READ_WRITE_EXEC(7u)
}
