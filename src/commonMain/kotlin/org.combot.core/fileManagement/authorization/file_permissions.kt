@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.combot.core.fileManagement.authorization

import org.combot.core.convertOctalToDecimal

/** Provides file permission meta data. Covers permissions for owner, group, and other. */
public data class FilePermissions(
    val owner: FilePermissionType,
    val group: FilePermissionType,
    val other: FilePermissionType
)

/**
 * Generates a file mode from [FilePermissions].
 * @return The file mode in decimal form.
 */
public fun FilePermissions.generateMode(): UInt =
    convertOctalToDecimal("${owner.num}${group.num}${other.num}".toInt()).toUInt()
