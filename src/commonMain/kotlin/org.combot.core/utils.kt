package org.combot.core

import kotlin.math.pow

internal fun convertOctalToDecimal(octal: Int): Int {
    var tmp = octal
    var decimalNumber = 0
    var i = 0
    while (tmp != 0) {
        decimalNumber += (tmp % 10 * 8.0.pow(i.toDouble()).toInt())
        ++i
        tmp /= 10
    }
    return decimalNumber
}
