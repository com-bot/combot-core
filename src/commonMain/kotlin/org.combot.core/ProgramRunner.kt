package org.combot.core

@Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")
/**
 * Base interface for running a program.
 */
public interface ProgramRunner {
    /** Name of the program. */
    public val programName: String
    /** The path to the program's directory. */
    public val programDir: String
    /** The arguments to use when running the program. */
    public val programArgs: Array<String>
    /** The path to the working directory. */
    public val workingDir: String

    /**
     * Runs the program in the foreground, which is typically used with frontend programs.
     * @param timeout How long the program can run before a timeout occurs. If set to *0* then no timeout occurs.
     * @return A Pair that contains the following:
     * 1. Output from the program.
     * 2. The program's return code. If the return code isn't *0* then a error has occurred with the program.
     */
    public suspend fun runInForeground(timeout: UInt = 8u): Pair<Array<String>, Int>

    /**
     * Runs the program in the background, which is typically used with backend programs.
     */
    public suspend fun runInBackground()

    /** Checks to see if the program is currently running. **/
    public suspend fun isRunning(): Boolean

    /**
     * Forces the program to exit if it is running.
     */
    public suspend fun terminate()

    /**
     * Changes an environment variable. Will set one if it doesn't exist.
     */
    public suspend fun changeEnvironmentVariable(name: String, value: String)

    /**
     * Gets the process ID of the program.
     * @return The process ID, or *-1* if the process ID couldn't be found.
     */
    public suspend fun fetchProcessId(): Int
}
